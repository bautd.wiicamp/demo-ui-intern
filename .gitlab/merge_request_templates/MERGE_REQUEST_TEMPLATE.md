# What's this Request do?
- What did you do?

# Impact range:
- What parts does it affect?

# Task link
- What is the link of the task?

# TODO Checkboxes
- [ ] Did you check UI is as same as Figma Design?
- [ ] Did you check Spec/Flow?
- [ ] Did you note influence scope + request link in sub task?

# Commands to run when checking request
- what is the command to run the request's checking?

# Screenshot (UI task)
